import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list-array',
  templateUrl: './list-array.component.html',
  styleUrls: ['./list-array.component.scss']
})
export class ListArrayComponent implements OnInit {
  @Input() arrayTareasHijo;
  @Output() borrarTareaHijo = new EventEmitter;
  @Output() editarTareaHijo = new EventEmitter;

  constructor() { }
  ngOnInit(): void {
  }
}