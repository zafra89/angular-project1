import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextNgclassComponent } from './text-ngclass.component';

describe('TextNgclassComponent', () => {
  let component: TextNgclassComponent;
  let fixture: ComponentFixture<TextNgclassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextNgclassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextNgclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
