import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-boton',
  templateUrl: './boton.component.html',
  styleUrls: ['./boton.component.scss']
})
export class BotonComponent implements OnInit {

  //@Input() innerText: string = 'Aceptar'; //Por defecto se creará como Aceptar
  @Input() themeBtn: string = 'green'; //Por defecto la variable será true

  constructor() { }

  ngOnInit(): void {
  }

  changeClass() {
    this.themeBtn = this.themeBtn === 'green'? 'red' : 'green';
  }
}