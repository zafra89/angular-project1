import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  galleryList = [
    {
      title: 'Snowboard',
      description: 'Paisaje de una estación de esquí nevada.',
      imgUrl: 'https://ssl.quiksilver.com/static/QS/default/category-assets/marketing-landing/landing/img/snow/tiles/snow_featured_1.jpg'
    },
    {
      title: 'Baloncesto',
      description: 'Imagen de un partido en Rucker Park (NY, USA)',
      imgUrl: 'https://forums.nba-live.com/dl_mod/thumbs/5776_Rucker%202K12%20Night.jpg'
    },
    {
      title: 'Fútbol',
      description: 'Fotografía del mítico estadio de Anfield Road (Liverpool).',
      imgUrl: 'https://e00-marca.uecdn.es/imagenes/2014/12/04/futbol/futbol_internacional/premier_league/1417710571_extras_noticia_foton_7_1.jpg'
    }
  ];
  constructor() { }
  addObjectPadre(e){
    this.galleryList.push({...e});
  }

  deleteObjectPadre(i){
    this.galleryList.splice(i, 1)
  }

  ngOnInit(): void {
  }
}