import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Output() anadirTareaHijo = new EventEmitter;
  nuevaTarea;
  ngOnInit(): void {
  }
}