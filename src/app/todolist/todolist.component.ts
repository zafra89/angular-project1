import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit {
  arrayTareasPadre = ['Estudiar', 'Programar'];

  constructor() { }

  ngOnInit(): void {
  }

  anadirTareaPadre($e) {
    this.arrayTareasPadre.push($e);
  }

  borrarTareaPadre($e) {
    this.arrayTareasPadre.splice($e, 1);
  }

  editarTareaPadre($e) {
    this.arrayTareasPadre[$e] = 'EDITADO CON EVENTO DE DOBLE CLICK'
  }
}