import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-gallery-blocks',
  templateUrl: './gallery-blocks.component.html',
  styleUrls: ['./gallery-blocks.component.scss']
})

export class GalleryBlocksComponent implements OnInit {

  @Input() galleryListHijo;
  @Output() deleteObjectHijo = new EventEmitter;


  constructor() {

  }

  ngOnInit(): void {
  }
}
