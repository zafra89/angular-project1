import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryBlocksComponent } from './gallery-blocks.component';

describe('GalleryBlocksComponent', () => {
  let component: GalleryBlocksComponent;
  let fixture: ComponentFixture<GalleryBlocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryBlocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryBlocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
