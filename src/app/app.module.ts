import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { XyzComponent } from './xyz/xyz.component';
import { XyzChildComponent } from './xyz-child/xyz-child.component';
import { FormComponent } from './form/form.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { BotonComponent } from './boton/boton.component';
import { ListArrayComponent } from './list-array/list-array.component';
import { TextNgclassComponent } from './text-ngclass/text-ngclass.component';
import { NgIfComponent } from './ng-if/ng-if.component';
import { TodolistComponent } from './todolist/todolist.component';
import { GalleryFormComponent } from './gallery-form/gallery-form.component';
import { GalleryBlocksComponent } from './gallery-blocks/gallery-blocks.component';
import { GalleryComponent } from './gallery/gallery.component';

@NgModule({
  declarations: [
    AppComponent,
    XyzComponent,
    XyzChildComponent,
    FormComponent,
    HelloWorldComponent,
    BotonComponent,
    ListArrayComponent,
    TextNgclassComponent,
    NgIfComponent,
    TodolistComponent,
    GalleryFormComponent,
    GalleryBlocksComponent,
    GalleryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
