interface Person {
  name: string,
  age: number
}

interface Student extends Person {
  rol: string,
  isEmployed: boolean
}

interface Teacher extends Person {
  rol: string,
  isEmployed: boolean
}

let student1: Student = {
  name: 'Jose',
  age: 30,
  rol: 'Student',
  isEmployed: false
}

let teacher1: Teacher = {
  name: 'Abel',
  age: 35,
  rol: 'Teacher',
  isEmployed: true
}