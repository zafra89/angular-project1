class Rectangulo {
  private base: number = 24;
  private altura: number = 99;


  constructor(base: number, altura: number) {
    this.base = base;
    this.altura = altura;
  }

  public calcularArea(): number {
    return this.base * this.altura;
  }
}

console.log('Base del RECTANGULO-1', new Rectangulo(2, 6).calcularArea());
console.log('Base del RECTANGULO-2', new Rectangulo(3, 9).calcularArea());