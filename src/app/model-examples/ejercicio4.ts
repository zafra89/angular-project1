class Videoconsola {
  nombre: string;
  color: string;
  portatil: boolean;

  constructor(nombre: string, color: string, portatil: boolean = false) {
    this.nombre = nombre;
    this.color = color;
    this.portatil = portatil;
  }
}

class Xbox extends Videoconsola {
  modoOnline: boolean;

  constructor(nombre: string, color: string, portatil: boolean = false, modoOnline: boolean) {
    super(nombre, color, portatil);
    this.modoOnline = modoOnline;
  }
}

class PlayStation extends Videoconsola {
  modoOnline: boolean;
  precioModoOnline: number;

  constructor(nombre: string, color: string, portatil: boolean = false, modoOnline: boolean, precioModoOnline: number = 30) {
    super(nombre, color, portatil);
    this.modoOnline = modoOnline;
    this.precioModoOnline = precioModoOnline;
  }
}

class GameBoy extends Videoconsola {
  numPilas: number;

  constructor(nombre: string, color: string, portatil: boolean = true, numPilas: number) {
    super(nombre, color, portatil);
    this.numPilas = numPilas;
  }
}

const xbox: Xbox = new Xbox('Xbox 360', 'Blanca', false, true);
const playstation2: PlayStation = new PlayStation('Play Station 2', 'Negra', false, false, 0);
const playstation4: PlayStation = new PlayStation('Play Station 4', 'Negra', false, true);
const gameboy: GameBoy = new GameBoy('Game Boy Color', 'Roja', undefined, 2);

console.log(xbox);
console.log(playstation2);
console.log(playstation4);
console.log(gameboy);