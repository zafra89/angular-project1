import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-gallery-form',
  templateUrl: './gallery-form.component.html',
  styleUrls: ['./gallery-form.component.scss']
})
export class GalleryFormComponent implements OnInit {

  @Output() addObjectHijo = new EventEmitter; 
  newObject = {
    title: '',
    description: '',
    imgUrl: ''
  };

  constructor() {

   }

  ngOnInit(): void {

  }
}